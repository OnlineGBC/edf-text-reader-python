from flask import Flask, flash, request, redirect, render_template, make_response
from flask_socketio import SocketIO, send, emit
from subprocess import Popen, PIPE
import json

from os import listdir
from os.path import isfile, isdir, join

import eventlet
import threading
import sys
import csv_process

eventlet.monkey_patch() 

def save_settings(csv_path, password):
    f = open("settings.txt","w+")
    f.write(csv_path+"\n")
    f.write(password)
    f.close()

def read_settings():
    csv_path, password = '',''
    try:    
        f = open("settings.txt","r")
        if f.mode == "r":            
            lines = f.readlines()
            if len(lines):
                csv_path = lines[0].rstrip()
            if len(lines) == 2:
                password = lines[1]
            f.close()
    except:
        pass
    return [csv_path, password]

def csv_files(path):
    if len(path)==0 or not isdir(path):
        return []
    return [f for f in listdir(path) if isfile(join(path, f)) and f.endswith(".csv")]

bin_path = "csv_process.py"
app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
app.config['csv_path'] = '..'
app.config['password'] = 'password'
socketio = SocketIO(app, async_mode='eventlet', ping_timeout=600, ping_interval=600)
thread_lock = threading.Lock()
thread = None
@app.route("/")
def index():
   return render_template("index.html")


@app.route("/files", methods=['GET'])
def get_files():
    return make_response(json.dumps({
        'fileNames': csv_files(app.config['csv_path']),
        'csvPath':app.config['csv_path']
    })) 

@app.route("/settings", methods=['GET','POST'])
def set_settings():
    data = json.loads(request.data)
    app.config['password'] = data['password']
    new_csv_path = data['csv_path']
    if not isdir(new_csv_path):
        return make_response('This path does not exist',300)
    filenames = csv_files(new_csv_path)
    print(filenames)
    if len(filenames) == 0:
        return make_response('Directory does not contains csv files',300)
    app.config['csv_path'] = new_csv_path
    save_settings(app.config['csv_path'],app.config['password'])        
    return make_response(json.dumps({
        'fileNames': filenames
    }))  

def process_out(line):
    socketio.emit('log', line.rstrip()) 
    eventlet.sleep(0)
    print(line.rstrip())

def run_code_thread(arg):
    full_path = join(app.config['csv_path'], arg['file'])   
    command_line = [bin_path, full_path, app.config['password']]    
    for email in arg['emails']:
        command_line.append(email) 
    csv_process.check_and_process_file(command_line, process_out)
    app.config['PROCESS'] = None


@app.route("/run", methods=['POST'])
def run_this():
    data = json.loads(request.data)    
    if 'PROCESS' not in app.config or app.config['PROCESS'] is None:
        socketio.start_background_task(target=run_code_thread, arg=data)
    return make_response('ok',200)  



if __name__ == '__main__':
    settings = read_settings()
    
    app.config['csv_path'] = settings[0]
    app.config['password'] = settings[1]
    if not isdir(app.config['csv_path']) or  len( csv_files(app.config['csv_path'])) == 0:
        app.config['csv_path'] = ''
    if len(sys.argv) == 2:
        if not isdir(sys.argv[1]):
            print(sys.argv[1]+" is not directory")
        else:
            app.config['csv_path'] = sys.argv[1]
            save_settings(sys.argv[1], app.config['password'])    
    
    if len(app.config['csv_path']) and len( csv_files(app.config['csv_path'])) == 0:
        print("Directory \""+ app.config['csv_path'] +"\" not contain csv file")
    else:            
        socketio.run(app,host='0.0.0.0',port=50880)
            
