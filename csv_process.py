from os.path import isfile, isdir, join, exists
import sys
import time
from datetime import datetime
import smtplib
from email.message import EmailMessage


def int_timestamp():
    return int(time.mktime(datetime.now().timetuple()))

print_emails = False #If print_emails = True, emails will display/print in stdout. 
                     #If print_emails = False, emails will not display/print in stdout.
email_from_str = "FUN-MedAlert@onlinegbc.com"
password_str = "1qaz@WSX1qaz@WSX"
subject_email = "Alert!"
state_str =  "Your Alert has been triggered because the medical threshold has been reached. Please check the patient immediately!!!\n"
too_many_emails_message ="More than 10 matching criteria have been observed in the last 15 minutes.  Please have a medical provider check the patient.  To avoid spam, no further alerts will be sent\n"
smtp_domain = "smtp.smtp2go.com"
smtp_port = 2525
interval_spam = 15 * 60
max_spam = 9

email_to_times = dict()

def check_file(file_path, print_fn):
    if not exists(file_path):
        print_fn("File {} not exist".format(file_path))
        return False
    if not isfile(file_path):
        print_fn(file_path, " not is file")
        return False
    if not file_path.endswith(".csv"):
        print_fn(file_path, " not is csv file")
        return False    
    return True

def check_line(line):
    values = line.split(',',5)
    if len(values) >= 4 and values[2] > 2 * values[3]:
        return True
    return False

def check_many_emails(email_to, last=False):
    timestamp = int_timestamp()
    if email_to not in email_to_times:
        return False
    sended_in_interval = len([val for val in email_to_times[email_to] if (val + interval_spam > timestamp)])
    if sended_in_interval >= (max_spam + int(last)):
        return True
    return False

def save_email_time(email_to):
    timestamp = int_timestamp()
    if email_to not in email_to_times:
        email_to_times[email_to] = [timestamp]
    else:
        email_to_times[email_to].append(timestamp)

def send_email(email_from, password, emails_to, message, print_fn):
    msg = EmailMessage()
    msg.set_content(message)
    msg['Subject'] = subject_email
    msg['From'] = email_from
    msg['To'] = emails_to
    
    if print_emails:
        print_fn(msg.get_content())
    try:
        server = smtplib.SMTP(smtp_domain, smtp_port)
        server.ehlo()
        server.starttls()
        server.login(email_from, password)
        server.send_message(msg)
        server.quit()
    except smtplib.SMTPAuthenticationError as e:
        print_fn('SMTP Authentication Error')
        return False    
    except smtplib.SMTPException as e:
        print_fn('SMTP error: {}'.format(e.args))
        return False
    return True


def send_messages(email_from, password, emails_to, message, print_fn):
    recipients, recipients_end = [], []
    for email_to in emails_to:
        if check_many_emails(email_to,True): # True  - too_many_emails_message was sended
            continue
        if check_many_emails(email_to):
            recipients_end.append(email_to)
            save_email_time(email_to)  
            continue
        recipients.append(email_to)
        save_email_time(email_to)
    
    if len(recipients):
        if not send_email(email_from, password, recipients, message, print_fn):            
            return False        

    if len(recipients_end):
        if not send_email(email_from, password, recipients_end, too_many_emails_message, print_fn):
            return False
        if len(emails_to) == len (recipients_end):
            print_fn(too_many_emails_message)
            print_fn("Now counting statistics for analysis -- please wait")
            return False
    return True    

def process_file(file_path, password, emails_to, print_fn):
    global email_to_times
    email_to_times = {}
    if not check_file(file_path, print_fn):
        return
    print_fn("Now evaluating ...")
    start = datetime.now()
    lines_by_conditions = 0
    send = True
    with open(file_path) as fp:
        line = fp.readline()
        cnt = 1
        while line:            
            line = fp.readline()
            if check_line(line):
                lines_by_conditions +=1
                message = state_str + line + "\n\n"
                if send and not send_messages(email_from_str, password, emails_to, message,print_fn):                    
                    send = False
                    
            cnt += 1
    end = datetime.now()
    ms = (end-start).microseconds/1000
    print_fn("Processed {} lines".format(cnt))
    print_fn("Found {} lines by conditions". format(lines_by_conditions))
    print_fn("Elapsed time: {} milliseconds".format(ms))
    return 0


def check_and_process_file(params, print_fn):
    argc = len(params)
    args_not_provided = False
    file_path = ''
    password = password_str
    if argc < 4:
        args_not_provided = True
    else:
        file_path = params[1]
        if len(params[2]):
            password = params[2]
        emails_to = params[3:]
        if not len(emails_to) or not len(emails_to[0]):
            args_not_provided = True
    if args_not_provided:
        print_fn("Please specify input file and email list")
        print_fn("Example: csv-read file.csv password email1@example.co email2@example.com ...")
    process_file(file_path, password, emails_to, print_fn)
    

if __name__ == '__main__':
    check_and_process_file(sys.argv, lambda x: print("out:",x))