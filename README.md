# edf-text-reader-Python

Python Program to read EDF files that have been converted to text

Written by Sergey Musikhin

Based on https://github.com/rwieruch/minimal-react-webpack-babel-setup

Designed by Raja Gopalan, raja@onlinegbc.com

All copyrights and licensing vested with their original authors and licensees

Residual and contracted code is Copyright Global Business Consulting, Inc.

909 Third Ave #222, NY, NY 10150
+1 800-351-8246
https://www.onlinegbc.com

## Run Locally

Clone this repo: "git clone git@gitlab.com:OnlineGBC/edf-text-reader-python.git"

Login as rpaauto

Install cmake and npm.

Run these commands:

    npm install
    npm run dev
    cd /home/rpaauto/RPA/edf-text-reader-python
    pip3 install -r ./requirements.txt
    nohup python3 ./server.py &
        #or python3 server.py /path/to/csv_files

Goto http://localhost:50880

If you would like to have webpack rebuild your javascript any time your React code changes, enter `npm run start` in a different terminal.

To update the SSL certificate, do the following:
1.  Purchase the certificate from a trusted Certificate Authority (CA).  The current wildcard certificates are with InterServer, sent by email on Dec 21, 2020, and stored on server funauto.onlinegbc.com:/home/rpaauto/RPA/fa-express/ssl/*
2.  cd /usr/local/share/ca-certificates/python
3.  Copy files ca_bundle.crt, certificate.crt and private.key to that location (rename them as necessary)


Troubleshooting:
If we get the issue:
502 Bad Gateway
nginx/1.14.0 (Ubuntu)

Solution:  
Check that the Python has been brought up using the "nohup" line above


How HTTPS is setup

nginx runs on the same server (dspauto.onlinegbc.com) and points to port 50880
Config files are saved in /etc/nginx/nginx.conf
This points to where the SSL fles are stored in //usr/local/share/ca-certficates/python
Approx around 2022 Jan, we will need to replace and buy new certificates

To start/stop nginx, the command:
sudo systemctl status nginx
sudo systemctl start nginx
sudo systemctl stop nginx




