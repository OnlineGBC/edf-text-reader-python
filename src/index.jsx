import React from 'react';
import ReactDOM from 'react-dom';
import {  get , post} from 'axios';
import openSocket from 'socket.io-client';
import {Container,Button, Select, TextField, InputLabel} from '@material-ui/core'


const apiHost = '';
const apiFiles = apiHost + '/files';
const apiSettings = apiHost + '/settings';
const apiRun = apiHost + '/run';
const socket = openSocket(apiHost, {transports: ['websocket']});



class App extends React.Component {
    componentWillMount() {
        
    }

    constructor(props) {
        super(props);
        this.state = {
          fileNames: [],
          currentFileIndex: 0,
          email_list:[],
          password: "",
          csv_path: "",
          out: []     
        };
        this.handleChangeEmailList = this.handleChangeEmailList.bind(this);
        this.handleSend = this.handleSend.bind(this);
        this.handleChangeFile = this.handleChangeFile.bind(this);
        this.handleChangeCsvPath = this.handleChangeCsvPath.bind(this);
        this.handleChangePassword = this.handleChangePassword.bind(this);
        this.handleApply = this.handleApply.bind(this);
      }

    



    getFiles()
    {
        var self = this;
        get(apiFiles)
        .then(function(response) {
          self.setState({
            fileNames:response.data.fileNames,
            csv_path: response.data.csvPath

          });

        }).catch(function (error) {
          console.log(error);
        })
        .then(function () {
          // always executed
        });
    }
    
    componentDidMount(){
        this.getFiles();
        socket.on("log",data => this.addLine(data));
    
      }
      
    handleChangeFile(event)
    {
        this.setState({currentFileIndex : event.target.value})
        event.preventDefault();
    }  

    handleChangeEmailList(event)
    {
        this.setState({email_list : event.target.value.split(" ")});
        event.preventDefault();
    }

    handleChangePassword(event)
    {
        this.setState({password : event.target.value});
        event.preventDefault();
    }

    handleChangeCsvPath(event)
    {
        this.setState({csv_path : event.target.value});
        event.preventDefault();
    }

    handleSend(event){
        this.clearOut();
        var self = this;
        post(apiRun, {
          file: this.state.fileNames[this.state.currentFileIndex],
          emails: this.state.email_list,
          password: this.state.password
        })
        .then(function(response) {
        }).catch(function (error) {
          self.clearOut();
          self.addLine(error.response.data);
        })
        .then(function () {
          // always executed
        });
    }

    handleApply(){
      var self = this;
      post(apiSettings, {
        csv_path: self.state.csv_path, password: self.state.password
      })
      .then(function(response) {
        self.setState({fileNames:response.data.fileNames});
        self.clearOut();
      }).catch(function (error) {
        self.clearOut();
        self.addLine(error.response.data);
      })
      .then(function () {
        // always executed
      });
    }

    clearOut()
    {
        this.setState({out: []});
    }
    
    addLine(line)
    {
        if(line == 'start')
            this.clearOut();
        else
            this.setState({out: this.state.out.concat([line])});
    }

    render() {
        const fileNames = this.state.fileNames;

        return (
            
            <div >
                <center><h3>Welcome to the FUN Medical Alert System!</h3></center>
                
                  <h5>Settings</h5>
                  <TextField
                         fullWidth = "true"
                         label="Define directory location of the input csv file"
                         type="text" 
                         value = {this.state.csv_path}                        
                         onChange={this.handleChangeCsvPath}
                         />
                         
                  <TextField
                  fullWidth = "true"
                  label="Password"
                  type="password"                          
                   onChange={this.handleChangePassword}
                  /><br/><br/>
                  <Button 
                variant="contained"
                color="primary"
                onClick={this.handleApply} fullWidth = "true">Apply</Button>
                  <br/><br/>
                <InputLabel htmlFor="file-csv-select">File</InputLabel>
                <Select native
                        value = {this.state.currentFileIndex}
                        onChange = {this.handleChangeFile}
                        fullWidth = "true"
                        inputProps={{
                            name: 'file',
                            id: 'file-csv-select',
                          }}>
                    {Object.keys(fileNames).map((key)=> <option value={key}>{fileNames[key]}</option>)}
                </Select>
                <div></div>
                <div>                  
                   <TextField
                         multiline
                         rowsMax="4"
                         fullWidth = "true"
                         label="Emails"
                         type="text" 
                         helperText = "Please enter a maximum of 10 emails separated by spaces"

                         onChange={this.handleChangeEmailList}/>
                </div>

                <br/>
                <Button 
                variant="contained"
                color="primary"
                onClick={this.handleSend} fullWidth = "true">Send</Button>
                <br/>
                <br/>
                <div>
                    {this.state.out.map((line)=> <div>{line}</div>)}
                </div>
                
            </div>
        );
    }
}

ReactDOM.render(<App />, document.getElementById('app'));
